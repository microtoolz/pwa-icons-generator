# pwa icons generator

Script to generate icons for VueJS pwa app from a PNG source (source should be at least 512 x 512) 

## HOW TO

### prerequistes
1. python is installed: `python3 --version`
2. install **PILLOW** lib: `python3 -m pip install --upgrade Pillow`
3. get more info about [pillow here](https://pillow.readthedocs.io/en/latest/installation.html)
4. Have a square image in **.png** with min 512px size

###
1. clone or dowload the project
2. open your terminal
3. type `python3` + space
4. drag and drop the script `resize_pwa_icon.py` on the terminal
5. press `ENTER` key
6. it will ask for the source ....
7. drag and drop your image on the terminal
8. press `ENTER` key
9. Wait ... the job should be done **🎉🎉 JOB DONE 🎉🎉**
10. your files are behin the source file.


![Step 1][img1]
![Step 1][img2]
![Step 1][img3]

[img1]: ./doc_media/1.png "Add your image"
[img2]: ./doc_media/2.png "Add your image"
[img3]: ./doc_media/3.png "Add your image"