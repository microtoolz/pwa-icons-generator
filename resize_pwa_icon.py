#!/usr/bin/env python3

# README:
#  
# Install PILLOW LIBRARY
# https://pillow.readthedocs.io/en/latest/installation.html
# ------------------------------------------
# python3 -m pip install --upgrade pip
# python3 -m pip install --upgrade Pillow
# ------------------------------------------
#

import os
from PIL import Image

print()
print("------------------------------")
print("Icon resizer for your PWA app!")
print("------------------------------")
print()

original_img = input("Originale source (in png): ").strip()
destination = os.path.dirname(original_img)

img = Image.open(original_img)
img_name = [
  { "name": 'android-chrome-192x192.png', "size": 192 },
  { "name": 'android-chrome-512x512.png', "size": 512 },
  { "name": 'android-chrome-maskable-192x192.png', "size": 192 },
  { "name": 'android-chrome-maskable-512x512.png', "size": 512 },
  { "name": 'apple-touch-icon-60x60.png', "size": 60 },
  { "name": 'apple-touch-icon-76x76.png', "size": 76 },
  { "name": 'apple-touch-icon-120x120.png', "size": 120 },
  { "name": 'apple-touch-icon-152x152.png', "size": 152 },
  { "name": 'apple-touch-icon-180x180.png', "size": 180 },
  { "name": 'apple-touch-icon.png', "size": 180 },
  { "name": 'favicon-16x16.png', "size": 16 },
  { "name": 'favicon-32x32.png', "size": 32 },
  { "name": 'msapplication-icon-144x144.png', "size": 144 },
  { "name": 'mstile-150x150.png', "size": 150 },
]

print()
print("Generating icons ...")
for target in img_name:
  size = target["size"]
  name = target["name"]
  new_image = img.resize((size,size), Image.ANTIALIAS)
  new_image.save(destination + "/" + name)
print("Icons generated")

print()
print("Generating .ico file ...")
img.save(destination + "/"+ "favicon.ico",format = 'ICO', sizes=[(32,32)])
print(".ico file generated")

print()
print(' 🎉🎉 JOB DONE 🎉🎉')
print()
print('files was save behind the source: ' + destination)
print()
